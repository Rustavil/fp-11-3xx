module Task204 where

numbers = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]

-- Возвращает колличество комбинации, не превышающих Х, и не включая единицу
count :: [Int] -> Int -> Integer
count [] _ = 0
count (a:as) x = go x
    where go x
              | (x >= a)  = 1 + go (div x a) + count as x
              | otherwise = 0


main = print $ (1+) $ count numbers (10^9)