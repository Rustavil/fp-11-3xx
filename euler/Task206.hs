  module Task206 where



  is_result ['1',_, '2',_, '3',_, '4',_, '5',_, '6',_, '7',_, '8',_, '9',_, '0'] = True
  is_result _ = False

  main = print $ filter (is_result . show . square) $ filter g [low, low+10..high]
      where low = 1000000030
            high = 1500000070
            square x = x * x
            g x = y == 30 || y == 70
                where y = x `mod` 100