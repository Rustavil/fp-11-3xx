module Task230 where

	--Расмотрим пример:

	--Фибоначи слова: а и б

	--fib(n) - Возвращает n-ое слово


	--шаг   |   слово
	--1     | а
	--2     | б
	--3     | аб 
	-- ...
	--n-2   | fib(n-2)
	--n-1   | fib(n-1)
	--n     | fib(n-2) и fib(n-1)

	--Итак, мы можем найти (n-1)-ое слово, убрав  n-2 из n.
	--Т. к. n-ое слово это последовательность сочетаний 1ого и 2ого слова,
	--i символ n-ого слова соответствует k-ому символу 1ого и 2ого слова.

	
	--Текущий алгоритм ораничен памятью
	import Data.Char

	--Возвращает шаг, на котором длина слова >= n (1,2,...)
	get_step :: Int -> Int -> Int -> Int
	get_step a b n | (n <= a) = 1
	               | otherwise = (get_step b (a+b) n) + 1

	--Возвращает длину слова на шаге
	get_size_in_step :: Int -> Int -> Int -> Int
	get_size_in_step a b s | (s == 1) = a
	                       | otherwise = (get_size_in_step b (a+b) (s-1))

	--Возвращает n символа на 2 шаге
	get_n_in_2step :: Int -> Int -> Int -> Int ->Int
	get_n_in_2step a b n s | (s == 3) = n
	                       | otherwise = get_n_in_2step a b (n - (get_size_in_step a b ((get_step a b n)-2))) (s-1)

	d :: String -> String -> Int -> Int
	d a b n | (length a) >= n = digitToInt (genericIndex a (n-1))
	          | (length b) >= n = digitToInt (genericIndex b (n-1))
	          | otherwise = digitToInt (genericIndex (a++b) ((get_n_in_2step len_a len_b n (get_step len_a len_b n))-1))
	               where len_a = length a
	                     len_b = length b

	main = print $ sum (map (\n->(10^n * (d a b ((127+19*n)*7^n)))) [0..17])


	--sum (map (\n->(10^n * (d a b ((127+19*n)*7^n)))) [0..17])


	{-import Prelude hiding ((!!))
	import Data.List (genericIndex)
	(!!) = genericIndex

	a = "1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679"
	b = "8214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196"


	fib :: Integer -> Integer
	fib = (100 *) . (map fib' [0..] !!)
	    where fib' = fst . fib2
	          fib2 0 = (1, 1)
	          fib2 1 = (1, 2)
	          fib2 n
	              | even n    = (a*a + b*b, c*c - a*a)
	              | otherwise = (c*c - a*a, b*b + c*c)
	              where (a,b) = fib2 (n `div` 2 - 1)
	                    c     = a + b

	-- Возвращает число, местоположение которого в слове Фибоначи будет меньше X
	locate x = let go i
	                   | fib i >= x = i
	                   | otherwise = go (i+1) in go 0


	d :: Integer -> Integer
	d x = read [go x $ locate x]
	    where go x 0 = a !! (x-1)
	          go x 1 = b !! (x-1)
	          -- Нахождение Хого символа в У слове Фибоначи
	          go x y
	              | u >= x = go x (y-2)
	              | otherwise = go (x-u) (y-1)
	              where u = fib (y-2)


	main = print $ sum (map (\n->(10^n * (d a b ((127+19*n)*7^n)))) [0..17])-}
	
