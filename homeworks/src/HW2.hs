-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown On = True
isKnown Off = True
isKnown Unknown = False
  

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
          deriving (Show, Eq)


eval :: Term -> Int
eval (Const x) = x
eval (Sub x y) = eval x - eval y
eval (Add x y) = eval x + eval y
eval (Mult x y) = eval x * eval y

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Sub x y) = Sub (simplify x) (simplify y)
simplify (Add x y) = Add (simplify x) (simplify y)

simplify (Mult (Add x y) z) = simplify (Add (Mult x z) (Mult y z))
simplify (Mult (Sub x y) z) = simplify (Sub (Mult x z) (Mult y z))
simplify (Mult x (Add z k)) = simplify (Add (Mult x z) (Mult x k))
simplify (Mult x (Sub z k)) = simplify (Sub (Mult x z) (Mult x k))

simplify (Mult x y) = simplify2(Mult (simplify x) (simplify y))

simplify2 (Mult (Add x y) z) = simplify (Add (Mult x z) (Mult y z))
simplify2 (Mult x (Add y z)) = simplify (Add (Mult x y) (Mult x z))
simplify2 (Mult (Sub x y) z) = simplify (Sub (Mult x z) (Mult y z))
simplify2 (Mult x (Sub y z)) = simplify (Sub (Mult x y) (Mult x z))
simplify2 t = t
