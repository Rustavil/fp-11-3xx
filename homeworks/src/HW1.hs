module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n | n == 1 = 1.0
        | otherwise =  hw1_2 (n-1) +(1/(fromIntegral n ^ fromIntegral n))

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | mod n 2 == 0 = product (filter (\x -> mod x 2 == 0) [2 .. n])
        | otherwise = product (filter (\x -> mod x 2 /= 0) [1 .. n])
-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime p | p == 1 = False
          | otherwise = sum (map (\x -> if mod p x == 0 then 1 else 0) [2..round (sqrt (fromIntegral p))]) == 0

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum $ filter isPrime [a..b]
