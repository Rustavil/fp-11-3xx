-- ��������� ������. ����� ���� ����������, ������� �� ����������, ������������ ���� ��� ����������� ����� 
data Term = Variable String
          | Lambda String Term
          | Application Term Term


-- ������ Show ��� ���� ������ Term
instance Show Term where
    show (Variable x) = x
    show (Application term1 term2) = (show term1) ++ " " ++ (show term2)
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")" 

-- �������� ���������� ������, ������� ��� � ������
-- ��������� ���� �� ���������� � ������,���� ��, �� ����� ���������� , ����� ���������
renameVariable :: [String] -> String -> Int -> String
renameVariable lst x n | elem (x ++ (show n)) lst = renameVariable lst x (n+1) 
                       | otherwise = x ++ (show n)

-- ����������� ���������� � ����, ��� ���� � ������ ���������������
-- ��������� ���� �� � ��� String � ������ ����������, ��  ������� �� ������ ��� ����������
-- ���� ��� �� ��������� 
renameFreeVariables :: [String] -> Term -> Term
renameFreeVariables lst (Lambda x term) | elem x lst = Lambda x (renameFreeVariables (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVariables lst term)
renameFreeVariables lst (Application t1 t2) = Application (renameFreeVariables lst t1) (renameFreeVariables lst t2)
renameFreeVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                                     | otherwise = Variable x


-- ������� ��� ������ ������ ���������� �� ������ ����
-- ���������� reduce, ���� ������ 
setVariable :: String -> [String] -> Term -> Term -> Term

setVariable var lst (Lambda x term) needTerm | x /= var = Lambda x (setVariable var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVariable var lst (Application term1 term2) needTerm = Application (setVariable var lst term1 needTerm) (setVariable var lst term2 needTerm)
setVariable var lst (Variable x) needTerm | x == var = renameFreeVariables lst needTerm
                                          | otherwise = Variable x  

-- ���� ��� � ������������ �����
eval1 :: Term -> Term
eval1 (Application (Lambda var res) t2) = setVariable var [] res t2
eval1 (Application (Application term1 term2) t2) = Application (eval1 (Application term1 term2)) t2
eval1 term = term


-- ������������ ����� ����� � ������������ �����
eval :: Term -> Term
eval (Application (Lambda var res) t2) = eval (setVariable var [] res t2)
eval (Application (Application term1 term2) t2) = eval (Application (eval (Application term1 term2)) t2)
eval term = term