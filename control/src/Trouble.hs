{-==== Нуркаев Руставиль, 11-303 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       , findMin
       , findMax
       ,getText
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket ls = if (snd max)==min
            then [max]
            else ls
  where
    max = findMax ls
    min = findMin ls

findMin :: [(Integer, Integer)] -> Integer
findMin a = (foldr (\(x,xs) (y) -> if y == (-1) then xs else (if xs < y then xs else y)) (-1) a)
findMax :: [(Integer, Integer)] -> (Integer, Integer)
findMax a = foldr (\(x,xs) (y1,y) -> if (quot x xs) > (quot y1 y) then (x,xs) else (y1,y)) (-1,-1) a



{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
getOrbiters :: [Load a] -> [Load a]
getOrbiters [] = []
getOrbiters ((Orbiter x):xs) = (Orbiter x):(getOrbiters xs)
getOrbiters ((Probe x):xs) = getOrbiters xs
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (getOrbiters x)
orbiters ((Rocket _ x):xs) = (orbiters xs) ++ (orbiters x)


{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier a = map getText a
getText :: Phrase -> String
getText (Warp c) = "Kirk"
getText (BeamUp s) = "Kirk"
getText (IsDead s) = "McCoy"
getText (LiveLongAndProsper) = "Spock"
getText (Fascinating) = "Spock"
